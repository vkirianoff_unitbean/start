<%@ page import="com.ub.core.authorization.routes.AuthorizationAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.ub.core.base.menu.BaseMenu" %>
<%@ page import="com.ub.core.base.routes.BaseRoutes" %>

<%--@elvariable id="adminMenuService" type="com.ub.core.pictureTask.view.all.SearchPictureTaskAdminViewModel"--%>
<%--@elvariable id="menu" type="com.ub.core.base.menu.BaseMenu"--%>
<%--@elvariable id="currentUser" type="com.ub.core.user.model.UserDoc"--%>
<%--@elvariable id="requestURL" type="String"--%>

<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation">
        <li class="user-details cyan darken-2">
            <div class="row">
                <div class="col col s12 m12 l12">
                    <ul id="profile-dropdown" class="dropdown-content">
                        <li>
                            <a href="<%=AuthorizationAdminRoutes.LOGOUT%>">
                                <i class="mdi-hardware-keyboard-tab"></i>
                                <s:message code="ubcore.admin.exit" text="Logout"/>
                            </a>
                        </li>
                    </ul>
                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn"
                       data-activates="profile-dropdown">
                        ${currentUser.email} <i class="mdi-navigation-arrow-drop-down right"></i>
                    </a>
                    <p class="user-roal"><s:message code="ubcore.admin.role" text="Administrator"/></p>
                </div>
            </div>
        </li>
        <li class="bold">
            <a href="<%=BaseRoutes.ADMIN%>" class="waves-effect waves-cyan">
                <i class="mdi-action-dashboard"></i> <s:message code="ubcore.admin.dashboard" text="Dashboard"/>
            </a>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <c:forEach items="<%= BaseMenu.getAll() %>" var="menu">
                    <c:if test="${currentUser.containsAnyRole(menu.roleNames)}">
                        <li class="bold">
                            <a <c:if test="${fn:length(menu.children) > 0}">class="collapsible-header waves-effect waves-cyan ${requestURL eq menu.url or menu.childrenContainsUrl(requestURL) ? "active" : ""}"</c:if>
                                    <c:if test="${fn:length(menu.children) eq 0}">href="${menu.url}"</c:if>>
                                <i class="${menu.icon}"></i> ${menu.name}
                            </a>
                            <c:if test="${fn:length(menu.children) > 0}">
                                <div class="collapsible-body">
                                    <ul>
                                        <c:forEach items="${menu.children}" var="menu">
                                            <li class="${requestURL eq menu.url ? "active" : ""}">
                                                <a href="${menu.url}">${menu.name}</a>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </c:if>
                        </li>
                    </c:if>
                </c:forEach>
            </ul>
        </li>
    </ul>
    <a class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"
       data-activates="slide-out">
        <i class="mdi-navigation-menu"></i>
    </a>
</aside>