<%@ page import="com.ub.start.blog.route.BlogRoute" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

    <link rel="stylesheet" href="/static/start/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="static/ub/css/main.css">
</head>
<body>
<div class="head">
    <div class="blog-logo"></div>
    <div class="head-count">Статьи, ${search.getAll()}</div>
    <a href="/create" class="head-blog-create"></a>
</div>
<div class="content">
<c:forEach items="${search.result}" var="doc" varStatus="status">
    <div class="blog-item">
        <img src="/pics/${doc.picId}" class="blog-item-background" />
        <div class="blog-item-title">${doc.title}</div>
        <div class="blog-item-date"><fmt:formatDate value="${doc.createdAt}" /></div>
        <div class="blog-item-hr"></div>
        <div class="blog-item-text">${doc.description}</div>
        <c:url value="<%=BlogRoute.ARTICLE%>" var="editUrl">
            <c:param name="id" value="${doc.id}"/>
        </c:url>
        <a href="${editUrl}" class="blog-index-article-btn" >Ещё...</a>
    </div>

</c:forEach>
</div>

<div class="section">
    <div class="row center-align">
        <ul class="pagination">
            <c:url value="<%=BlogRoute.INDEX%>" var="prevUrl">
                <c:param name="query" value="${search.query}"/>
                <c:param name="currentPage" value="${search.prevNum}"/>
            </c:url>
            <li class="${search.prevNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a <c:if test="${search.prevNum ne search.currentPage}">href="${prevUrl}"</c:if>>
                    <i class="mdi-navigation-chevron-left"></i>
                </a>
            </li>

            <c:forEach items="${search.pagination}" var="page">
                <c:url value="<%=BlogRoute.INDEX%>" var="pageUrl">
                    <c:param name="query" value="${search.query}"/>
                    <c:param name="currentPage" value="${page}"/>
                </c:url>
                <li class="${search.currentPage eq page ? 'active' : ''}">
                    <a href="${search.currentPage ne page ? pageUrl : ''}">${page + 1}</a>
                </li>
            </c:forEach>

            <c:url value="<%=BlogRoute.INDEX%>" var="nextUrl">
                <c:param name="query" value="${search.query}"/>
                <c:param name="currentPage" value="${search.nextNum}"/>
            </c:url>
            <li class="${search.nextNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a <c:if test="${search.nextNum ne search.currentPage}">href="${nextUrl}"</c:if>>
                    <i class="mdi-navigation-chevron-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>

</body>
</html>
