<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set value="${doc}" var="doc" scope="request"/>

<head>
    <title>${doc.title}</title>
    <link rel="stylesheet" href="/static/start/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
<div class="content">
        <div class="blog-article-item">
            <img src="/pics/${doc.picId}" class="blog-article-item-background" />
            <div class="blog-article-item-title">${doc.title}</div>
            <div class="blog-article-item-date"><fmt:formatDate value="${doc.createdAt}" /></div>
            <div class="blog-article-item-text">${doc.description}</div>
            <a href="/" class="blog-article-btn">Назад</a>
        </div>
</div>


</body>
</html>
