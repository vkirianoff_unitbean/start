<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.ub.start.blog.route.BlogRoute" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/static/start/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>

<div class="content mod-create">
    <c:set value="${doc}" var="doc" scope="request"/>
    <div class="blog-create">
        <form:form action="<%= BlogRoute.SUCCESS %>" class="card-panel" modelAttribute="doc" method="post" enctype="multipart/form-data">
            <h1 class="blog-create-h1">Добавление статьи</h1>

            <div class="blog-create-group">
                <lable class="blog-create-label">Заголовок</lable>
                <form:input path="title" id="title" class="blog-create-input"/>
            </div>

            <div class="blog-create-group">
                <lable class="blog-create-label">Описание</lable>
                <form:textarea path="description" id="description" class="blog-create-input mod-textarea"></form:textarea>
            </div>

            <div class="blog-create-footer">
                <lable class="blog-create-label">Вставте изображение</lable>
                <div class="blog-create-file">
                    <input name="pic" type="file" class="blog-create-file-input"/>
                </div>

                <button class="btn btn-approve" name="action">Добавить</button>
                <a href="/" class="btn btn-cancel">Отмена</a>
            </div>
        </form:form>
    </div>
</div>
</body>
</html>
