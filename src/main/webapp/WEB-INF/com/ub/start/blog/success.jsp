
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/static/start/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>

<div class="content">
    <div class="success">
        <img class="success-logo" src="/static/start/img/152.png"></img>
        <h1 class="success-title">Поздравляем Вас с успешным добавлением статьи в блог UnitBean!</h1>
    </div>
</div>
</body>
</html>
