package com.ub.start.blog.route;

public class BlogRoute{
    public static final String INDEX ="/";
    public static final String CREATE = "/create";
    public static final String SUCCESS = "/success";
    public static final String ARTICLE = "/article";
}
