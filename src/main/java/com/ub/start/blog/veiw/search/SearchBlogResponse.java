package com.ub.start.blog.veiw.search;

import com.ub.core.base.view.search.SearchResponse;
import com.ub.start.blog.model.BlogDoc;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;

import java.util.List;

public class SearchBlogResponse extends SearchResponse {

    private List<BlogDoc> result;

    public SearchBlogResponse(){}

    public List<BlogDoc> getResult() {
        return result;
    }

    public void setResult(List<BlogDoc> result) {
        this.result = result;
    }

    public SearchBlogResponse(Integer currentPage, Integer pageSize, List<BlogDoc> result){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;

    }
}
