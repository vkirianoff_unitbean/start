package com.ub.start.blog.veiw.search;

import com.ub.core.base.view.search.SearchRequest;

public class SearchBlogRequest extends SearchRequest{
    public  SearchBlogRequest(){
        this.setPageSize(10);
    }

    public  SearchBlogRequest(Integer currentPage){
        this.currentPage = currentPage;
        this.setPageSize(10);
    }

    public  SearchBlogRequest(Integer curentPage, Integer pageSize){
        this.currentPage = curentPage;
        this.pageSize = pageSize;

    }
}
