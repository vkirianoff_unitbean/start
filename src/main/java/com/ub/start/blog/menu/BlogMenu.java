package com.ub.start.blog.menu;

import com.ub.core.base.menu.BaseMenu;
import com.ub.core.base.menu.MenuIcons;

public class BlogMenu extends BaseMenu {
    public BlogMenu() {
        this.name = "Болг";
        this.icon = MenuIcons.MDI_ACTION_DESCRIPTION;
    }
}